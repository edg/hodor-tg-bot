extern crate futures;
extern crate rand;
extern crate telegram_bot;
extern crate tokio_core;
#[macro_use]
extern crate log;
extern crate env_logger;

use futures::Stream;
use rand::Rng;
use telegram_bot::prelude::*;

fn main() {
    env_logger::init();
    let token = std::env::var("HODOR_TG_BOT_TOKEN").unwrap();
    let mut core = tokio_core::reactor::Core::new().unwrap();
    let api = telegram_bot::Api::configure(token)
        .build(core.handle())
        .unwrap();
    let future = api
        .stream()
        .then(|res| {
            if let Err(ref e) = res {
                error!("Error: {}", e);
            }
            let result: Result<Result<telegram_bot::Update, telegram_bot::Error>, ()> = Ok(res);
            result
        })
        .for_each(|res| {
            match res {
                Ok(update) => {
                    if let telegram_bot::types::UpdateKind::Message(message) = update.kind {
                        debug!("Received message {:?}", message);
                        let responses: [&'static str; 5] = [
                            "Hodor",
                            "*Hodor*!!!",
                            "Hodor! Hodor!",
                            "_Hodor..._",
                            "Hodor?",
                        ];
                        let mut rng = rand::thread_rng();
                        let response = *rng.choose(&responses).unwrap();
                        let must_reply = match message.chat {
                            telegram_bot::types::MessageChat::Private(_) => true,
                            _ => rng.gen_range(0, 100) == 0,
                        };
                        if must_reply {
                            debug!("Replying");
                            api.spawn(
                                message
                                    .chat
                                    .text(response)
                                    .parse_mode(telegram_bot::types::ParseMode::Markdown),
                            );
                        }
                    }
                }
                Err(e) => {
                    error!("Error: {}", e);
                }
            }
            Ok(())
        });
    info!("Starting loop");
    core.run(future).unwrap();
}
